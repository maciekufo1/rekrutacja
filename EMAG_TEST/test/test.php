
<form action="test.php">
    <input type="submit" value="Once more" />
</form>
<?php


require dirname(__DIR__) . '/vendor/autoload.php';

use Game\Npc;
use Game\Player;
use Game\Skills\Skill;

$battleground = [];
$player = new Player();
$battleground[] = $player->setName('Orderus')
    ->setHealth(rand(70,100))
    ->setStrength(rand(70,80))
    ->setDefence(rand(45,55))
    ->setSpeed(rand(40,50))
    ->setLuck(rand(10,30))
    ->learnSkill(Skill::RAPID_STRIKE)
    ->learnSkill(Skill::MAGIC_SHIELD)
    ->getId();

$npc = new Npc();
$battleground[] = $npc->setName("Wild beast")
    ->setHealth(rand(60,90))
    ->setStrength(rand(60,90))
    ->setDefence(rand(40,60))
    ->setSpeed(rand(40,60))
    ->setLuck(rand(25,40))
    ->getId();

$winner = '';
$fastestEntity = null;
$fastestEntityId = 0;
$fastestEntitySpeed = 0;
$fastestEntityLuck = 0;

$player->showStats($player->getId());
//$player->showSkills($player->getId());
$npc->showStats($npc->getId());

foreach ($battleground as $entityId){
    /** @var Npc $entity */
    $entity = Game\Entity::getEntityById($entityId);

    $entitySpeed = $entity->getSpeed();
    $entityLuck = $entity->getLuck();
    if($entitySpeed >= $fastestEntitySpeed){
        if($entitySpeed == $fastestEntitySpeed && $entityLuck < $fastestEntityLuck){
            break;
        }
        $fastestEntity = $entity;
        $fastestEntityId = $entityId;
        $fastestEntitySpeed = $entitySpeed;
        $fastestEntityLuck = $entityLuck;
    }
}

echo $fastestEntity->getName(). ' charges at the enemy!'. "<br>" . "<br>";

$numberOfTurns = 10;
for ($i = $fastestEntityId; $i < $numberOfTurns + $fastestEntityId; $i++){
    /** @var Npc $attackingNpc */
    $attackingNpc = \Game\Entity::getEntityById($battleground[$i%2]);
    /** @var Npc $defendingNpc */
    $defendingNpc = \Game\Entity::getEntityById($battleground[($i+1)%2]);

    $damageGiven = $attackingNpc->attack($defendingNpc->getId());

    echo $attackingNpc->getName() . " attacked " .$defendingNpc->getName() .  "<br>";
    echo $defendingNpc->getName() . " took " . $damageGiven . " damage" . "<br>";

    if (!$defendingNpc->isAlive()){
        echo ' '.$defendingNpc->getName() . " died!". "<br>";
        $winner = $attackingNpc->getName();
        break;
    }
    echo " he has remaining " . $defendingNpc->getHealth(). "<br>";
}
if($winner != ''){
    echo $attackingNpc->getName() . ' won this fight!'. "<br>";
}else{
    foreach ($battleground as $entityId){
        $entity = \Game\Entity::getEntityById($entityId);
        if(get_class($entity) == Npc::class){
            echo 'The ' . $entity->getName() . ' started running from our hero!'. "<br>";
        }
    }
}