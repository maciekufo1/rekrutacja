<?php

namespace Game\Skills;

use Game\Entity;
use Game\Npc;

class SkillRapidStrike extends Skill implements SkillInterface {

    public function __construct(int $id)
    {
        $this->setName('Rapid Strike');
        $this->setDescription('Strike twice while it\'s his turn to attack, there\'s a 10% he\'ll use this skill every time he attacks.');
        $this->setLuck(10);
    }

    public function castSkill(int $enemyId, array $params = [])
    {
        $damage = isset($params['damage'])?$params['damage']:0;
        $skillRoll = rand(0, 100);
        if($damage > 0 && $this->getLuck() >= $skillRoll){
            $damage = $damage * 2;
            $entity = Entity::getEntityById($enemyId);
            echo '[Skill]['.$skillRoll.'] '. $entity->getName(). ' has casted ' . $this->getName().PHP_EOL;
        }
        return $damage;
    }

    public function getSkillChance()
    {
        parent::getLuck();
    }
}