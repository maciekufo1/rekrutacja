<?php


namespace Game\Skills;

use Game\Npc;

interface SkillInterface
{
    public function __construct(int $skillId);

    public function castSkill(int $entityId, array $params = []);
    public function getSkillChance();
    public function getTriggerType();
}

