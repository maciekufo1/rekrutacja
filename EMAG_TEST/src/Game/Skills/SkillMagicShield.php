<?php

namespace Game\Skills;

use Game\Entity;
use Game\Npc;

class SkillMagicShield extends Skill implements SkillInterface {

    public function __construct(int $id)
    {
        $this->setName('Magic shield');
        $this->setDescription('Takes only half of the usual damage when an enemy attacks, there\'s a 20% change he\'ll use this skill every time he defends.');
        $this->setLuck(20);

        return $this;
    }

    public function castSkill(int $entityId, array $params = [])
    {
        $damage = $params['damage'];
        if($damage > 0){
            $skillRoll = rand(0, 100);
            if($this->getLuck() >= $skillRoll){
                $damage = $damage / 2;
                $entity = Entity::getEntityById($entityId);
                echo '[Skill]['.$skillRoll.'] '. $this->getName(). ' was casted on ' . $entity->getName()."<br>";
            }
        }
        return $damage;
    }

    public function getSkillChance()
    {
        parent::getLuck();
    }
}