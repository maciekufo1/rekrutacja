<?php


namespace Game\Skills;

use Game\Npc;

abstract class SkillFactory{

    private static $currentSkillId = 0;
    private static $skills = [];

    static function getSkillById ($id){
        return isset(self::$skills[$id])?self::$skills[$id]:-1;
    }

    static function createSkill(string $type, array $params = [])
    {
        $skill = "Game\\Skills\\Skill" . str_replace('_','',ucwords($type," \t\r\n\f\v_"));

        if(class_exists($skill))
        {
            self::$skills[self::$currentSkillId] = new $skill(self::$currentSkillId);
            $skillId = self::$currentSkillId;
            self::$currentSkillId++;
            return $skillId;
        }
        else {
            throw new \Exception("Invalid skill type given.[".$skill."]");
        }
    }

}