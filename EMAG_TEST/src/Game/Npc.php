<?php
/**
 * Created by PhpStorm.
 * User: uvale
 * Date: 06-May-18
 * Time: 08:48
 */

namespace Game;


use Game\Skills\Skill;
use Game\Skills\SkillFactory;
use Game\Skills\SkillInterface;

class Npc extends Entity
{
    protected $name;

    protected $health;
    protected $strength;
    protected $defence;
    protected $speed;
    protected $luck;

    protected $skills;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Npc
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function isAlive(){
        return $this->getHealth() > 0 ? true:false;
    }

    /**
     * @return mixed
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * @param mixed $health
     * @return Npc
     */
    public function setHealth($health)
    {
        $this->health = $health>0?$health:0;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * @param mixed $strength
     * @return Npc
     */
    public function setStrength($strength)
    {
        $this->strength = $strength;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDefence()
    {
        return $this->defence;
    }

    /**
     * @param mixed $defence
     * @return Npc
     */
    public function setDefence($defence)
    {
        $this->defence = $defence;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param mixed $speed
     * @return Npc
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLuck()
    {
        return $this->luck;
    }

    /**
     * @param mixed $luck
     * @return Npc
     */
    public function setLuck($luck)
    {
        $this->luck = $luck;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSkills()
    {
        return isset($this->skills)?$this->skills:[];
    }

    public function learnSkill(string $skillName){
        $currentLearnedSkillId = SkillFactory::createSkill($skillName);
        $this->skills[] = $currentLearnedSkillId;
        $currentLearnedSkill = SkillFactory::getSkillById($currentLearnedSkillId);
        echo $this->getName() . ' learned ' . $currentLearnedSkill->getName() . "<br>";
        echo 'Description: ' . $currentLearnedSkill->getDescription() ."<br>" ;

        return $this;
    }

    public function takeDamage($amount){
        $damage = abs($this->getDefence() - $amount);
        $dodgeRoll = rand(0, 100);
        if($this->getLuck() <= $dodgeRoll){
            if($damage > 0){
                /** @var SkillInterface $skill */
                foreach ($this->getSkills() as $skill){
                    /** @var SkillInterface $skill */
                    $skill = SkillFactory::getSkillById($skill);
                    if($skill->getTriggerType() == Skill::TRIGGER_ON_TAKE_DAMAGE){
                        $damage = $skill->castSkill($this->getId(),['damage' => $damage]);
                    }
                }
                $this->setHealth($this->getHealth() - $damage);
            }
        }else{
            echo $this->getName() . ' dodged['.$dodgeRoll.'] the attack.'.PHP_EOL;
        }

        return $damage;
    }

    public function attack(int $entityId){
        $damage = $this->getStrength();
        $enemy = Entity::getEntityById($entityId);
        /** @var SkillInterface $skill */
        foreach ($this->getSkills() as $skill){
            $skill = SkillFactory::getSkillById($skill);
            if($skill->getTriggerType() == Skill::TRIGGER_ON_ATTACK){
                $damage = $skill->castSkill($entityId,['damage' => $damage]);
            }
        }
        //Damage = Attacker strength - Defender defence
        return $enemy->takeDamage($damage);
    }
    public function showStats(int $entityId){

        echo "<br> STATS:<br>" ;
        Echo "name: ". $name=$this->getName();
        echo"<br>";
        echo $name." speed: ";
         echo $this->getSpeed()."<br>";
         echo $name." strenght: ";
         echo $this->getStrength()."<br>";
         echo $name." Luck: ";
         echo $this->getLuck()."<br>";
         echo $name." speed: ";
         echo $this->getDefence()."<br>";
         echo $name." defence: ";
         echo $this->getSpeed()."<br>";

    }
}